FROM ubuntu:18.04 as base

ARG user=builder

RUN apt-get update && apt-get install -y \
    build-essential \
    gcc-multilib \
    bison \
    flex \
    libgmp3-dev \
    libmpc-dev \
    libmpfr-dev \
    texinfo \
    wget \
    file \
    libisl-dev && apt-get clean

ENV PREFIX="/home/${user}/opt/cross"
ENV TARGET=i686-elf
ENV PATH=$PREFIX/bin:$PATH

RUN mkdir -p /home/${user}/src && \
    wget -q https://ftp.gnu.org/gnu/binutils/binutils-2.36.1.tar.xz -P /home/${user}/src && \
    wget -q https://ftp.gnu.org/gnu/gcc/gcc-10.3.0/gcc-10.3.0.tar.gz -P /home/${user}/src && \
    tar -xf /home/${user}/src/binutils-2.36.1.tar.xz -C /home/${user}/src && \
    tar -xf /home/${user}/src/gcc-10.3.0.tar.gz -C /home/${user}/src

WORKDIR /home/${user}/src/build-binutils
RUN ../binutils-2.36.1/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror && \
    make && make install


WORKDIR /home/${user}/src/build-gcc
RUN which -- $TARGET-as || echo $TARGET-as is not in the PATH && \
    ../gcc-10.3.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers && \
    make all-gcc && \
    make all-target-libgcc && \
    make install-gcc && \
    make install-target-libgcc

RUN rm -rf /home/${user}/src



FROM ubuntu:18.04 as build-chain

ARG user=builder

ENV PREFIX="/home/${user}/opt/cross"
ENV TARGET=i686-elf
ENV PATH=$PREFIX/bin:$PATH

RUN mkdir -p /home/${user}/src
RUN apt-get update && apt-get install make -y --no-install-recommends \
    libmpc-dev \
    libisl-dev \
    xorriso \ 
    grub-common && apt-get clean

COPY --from=base /home/${user}/opt/cross /home/${user}/opt/cross
WORKDIR /src

