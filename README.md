[![build_badge](https://gitlab.com/pinkfloydhun/VSD/badges/master/pipeline.svg)](https://gitlab.com/pinkfloydhun/VSD/-/commits/master}) [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# VSD - OSDev.org inspired hobby OS.
